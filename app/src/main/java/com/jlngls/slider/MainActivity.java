package com.jlngls.slider;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.heinrichreimersoftware.materialintro.app.IntroActivity;
import com.heinrichreimersoftware.materialintro.slide.SimpleSlide;

public class MainActivity extends IntroActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
     //   setContentView( R.layout.activity_main );


        addSlide( new SimpleSlide.Builder()
                .title( "titulo" )
                .description( "descriçāo" )
                .image( R.drawable.um )
                .background( R.color.colorAccent )
                .backgroundDark( android.R.color.holo_blue_dark )
                .build() );

        addSlide( new SimpleSlide.Builder()
                .title( "titulo" )
                .description( "descriçāo" )
                .image( R.drawable.dois )
                .background( R.color.colorAccent )
                .backgroundDark( android.R.color.holo_blue_dark )
                .build() );

    }
}
